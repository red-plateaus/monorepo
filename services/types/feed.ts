export type FeedResponse = FeedResult[]
export interface FeedResult {
  title: string
  id: string,
  description: string
  thumbnail: string
  channel: string
  date: number
}
