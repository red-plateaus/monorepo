import { LiteratureList as ILiteratureList, LiteratureItem as ILiteratureItem } from '../../common/types/literature-list'
import { model, Schema, Document } from 'mongoose'

const LiteratureItemSchema: Record<keyof ILiteratureItem, any> = {
  name: { type: String, required: true },
  authors: { type: [String], required: true },
  publicationYear: { type: Number, required: true },
  publisher: { type: String, required: true }
}

const LiteratureListSchema: Record<keyof ILiteratureList, any> = {
  name: { type: String, required: true },
  description: { type: String, required: true },
  literature: [{ type: Schema.Types.ObjectId, ref: 'LiteratureItem' }]
}

export interface LiteratureItemDocument extends Document<ILiteratureItem> {}
export interface LiteratureListDocument extends Document<ILiteratureList> {}

export const LiteratureItem = model<LiteratureItemDocument>('LiteratureItem', new Schema<LiteratureItemDocument>(LiteratureItemSchema))
export const LiteratureList = model<LiteratureListDocument>('LiteratureList', new Schema<LiteratureListDocument>(LiteratureListSchema))

export { default as mongoose } from 'mongoose'
