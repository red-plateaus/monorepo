import { Script as IScript } from '../../common/types/script'
import { model, Schema, Document } from 'mongoose'

const ScriptSchema: Record<keyof IScript, any> = {
  title: { type: String, required: true },
  body: { type: String, required: true },
  video: { type: String }
}

export interface ScriptDocument extends Document<IScript> {}
export const Script = model<ScriptDocument>('Script', new Schema<ScriptDocument>(ScriptSchema))
