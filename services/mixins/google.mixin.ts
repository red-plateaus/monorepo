import { google, GoogleApis, youtube_v3 } from "googleapis"

export default class GoogleServices{
  public google: GoogleApis = google
  private key: string = process.env.GOOGLE_KEY

  public constructor () {
    this.google.options({
      auth: this.key
    })
  }
}

