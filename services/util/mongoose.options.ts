import { ConnectOptions } from 'mongoose'

export const mongooseOptions: ConnectOptions = {
  useUnifiedTopology: true,
  useCreateIndex: true,
  useNewUrlParser: true,
  autoIndex: true
}
