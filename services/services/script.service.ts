import { Context, Service, ServiceBroker, ServiceSchema } from 'moleculer'
import DbConnection from '../mixins/db.mixin'
import { Script } from '../models/script'

export default class LiteratureListService extends Service {

	private DbMixin = new DbConnection(Script).start()

	// @ts-ignore
	public  constructor (public broker: ServiceBroker, schema: ServiceSchema<{}> = {}) {
		super(broker)

		this.parseServiceSchema(Service.mergeSchemas({
			name: 'script',
			mixins: [this.DbMixin],
			settings: {
				fields: [
					'_id',
					'body',
					'video',
					'title'
				]
			},
			methods: {
				async seedDB(this: Service) {
					/*await this.adapter.insertMany([
            {
              description: 'Essential reading for all coomunists',
              literature: await this.broker.call('literature-item.find'),
              name: 'Coomunist Essentials'
            },
					])*/
				}
			}
		}, schema))
	}
}
