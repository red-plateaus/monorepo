import { Context, Service, ServiceBroker, ServiceSchema } from 'moleculer'
import DbConnection from '../mixins/db.mixin'
import { LiteratureList } from '../models/literature-list'

export default class LiteratureListService extends Service {

	private DbMixin = new DbConnection(LiteratureList).start()

	// @ts-ignore
	public  constructor (public broker: ServiceBroker, schema: ServiceSchema<{}> = {}) {
		super(broker)

		this.parseServiceSchema(Service.mergeSchemas({
			name: 'literature-list',
			mixins: [this.DbMixin],
      dependencies: [
        'literature-item'
      ],
			settings: {
				fields: [
					'_id',
					'description',
					'literature',
					'name'
				]
			},
			methods: {
				async seedDB(this: Service) {
					await this.adapter.insertMany([
            {
              description: 'Essential reading for all coomunists',
              literature: await this.broker.call('literature-item.find'),
              name: 'Coomunist Essentials'
            },
					])
				}
			}
		}, schema))
	}
}
