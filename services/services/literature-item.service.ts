import { Context, Service, ServiceBroker, ServiceSchema } from 'moleculer'
import DbConnection from '../mixins/db.mixin'
import { LiteratureItem } from '../models/literature-list'

export default class LiteratureItemService extends Service {

	private DbMixin = new DbConnection(LiteratureItem).start()

	// @ts-ignore
	public  constructor (public broker: ServiceBroker, schema: ServiceSchema<{}> = {}) {
		super(broker)

		this.parseServiceSchema(Service.mergeSchemas({
			name: 'literature-item',
			mixins: [this.DbMixin],
			settings: {
				fields: [
					'_id',
					'authors',
					'name',
					'publicationYear',
          'publisher'
				]
			},
			methods: {
				async seedDB() {
					await this.adapter.insertMany([
            {
              authors: [
                'Friedrich Engels',
                'Karl Marx'
              ],
              name: 'Das Kapital',
              publicationYear: 1867,
              publisher: 'Verlag von Otto Meisner'
            },
					])
				}
			}
		}, schema))
	}
}
