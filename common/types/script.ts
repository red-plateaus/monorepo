export interface Script {
  title: string
  video: string
  body: string
}
