export interface LiteratureItem {
  publicationYear: number
  publisher: string
  name: string
  authors: string[]
}

export interface LiteratureList {
  name: string
  description: string
  literature: LiteratureItem[]
}
